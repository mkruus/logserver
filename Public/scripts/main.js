function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


var app = new Vue({
    el: '#app', data: {
                  logs: [{ id: "ok", category: "Observer", title: "Ready", action: "Connected", label: "Welcome!", date: new Date() }]
    }
})

var socket = new WebSocket("ws://" + window.location.host + "/test/" + guid())

socket.onmessage = function(event) {
    var text = event.data
    var json = JSON.parse(text)
    console.log(json)
    json.date = new Date()
    if (json.action) {
        app.logs.unshift(json)
    }
}
