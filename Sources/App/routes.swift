import Vapor

/// Register your application's routes here.

let broadcaster = Broadcaster.shared

public func routes(_ router: Router) throws {

    router.get("/") { req in
        return try req.view().render("Start.html")
    }

//    router.get("json") { req in
//        return LogData(id: "123", action: "HEJ HOPP", label: "Smurf")
//    }

    router.post("log") { (req) -> Future<HTTPStatus> in
        return try req.content.decode(LogData.self).map(to: HTTPStatus.self, { (data)  in
            broadcaster.broadcast(senderId: "logger", log: data)
            return .ok
        })
    }
}

public struct LogData: Content {
    public var id: String
    public var category: String
    public var title: String
    public var action: String
    public var label: String
}
