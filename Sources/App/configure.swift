import FluentSQLite
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first

    services.register(Server.self) { container -> NIOServer in
        var serverConfig = try container.make() as NIOServerConfig
        serverConfig.port = 8080
        serverConfig.hostname = "95.85.24.218"
        let server = NIOServer(
            config: serverConfig,
            container: container
        )
        return server
    }


    let broadcaster = Broadcaster.shared

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    let wss = NIOWebSocketServer.default()

    wss.get("test", String.parameter) { (ws, req) in
        let observerId = try req.parameters.next(String.self)
        print("Connected: \(observerId)")
        broadcaster.add(ws, id: observerId)
        ws.send("{\"status\": \"OK\", \"id\": \"\(observerId)\"}")
        ws.onText({ (ws, text) in
            let log = LogData.init(id: observerId, category: "Observer", title: observerId, action: "sent text", label: text)
            broadcaster.broadcast(senderId: observerId, log: log)
        })

        ws.onClose.always {
            broadcaster.remove(id: observerId)
            print("closed: \(observerId)")
        }
    }

    services.register(wss, as: WebSocketServer.self)
    /// Register middleware

    var middlewares = MiddlewareConfig()
    middlewares.use(FileMiddleware.self)
    services.register(middlewares)

    // Configure a SQLite database

    /// Register the configured SQLite database to the database config.

    /// Configure migrations

}
