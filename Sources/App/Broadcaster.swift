//
//  Session.swift
//  App
//
//  Created by Markus Kruusmägi on 2018-11-13.
//

import Foundation
import Vapor

class Broadcaster {

    private func info() {
        print("Observers: \n", connections.keys)
    }

    public static var shared = Broadcaster()

    private var connections: [String: WebSocket] = [:]

    func add(_ connection: WebSocket, id: String) {
        connections[id] = connection
        info()
    }

    func remove(id: String) {
        connections.removeValue(forKey: id)
        info()
    }

    func broadcast(senderId: String, log: LogData) {
        for (_, socket) in connections {
//            socket.send("From '\(senderId)': \(text)")
            let data = try! JSONEncoder().encode(log)
            let str = String(data: data, encoding: .utf8)!
            socket.send(str)
        }
    }
    
    private init() {

    }
}

